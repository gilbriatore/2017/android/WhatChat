package br.edu.up.whatchat;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

  static final int PORTA = 8888;
  TextView txtLocal;
  EditText txtRemoto;
  EditText txtChat;
  EditText txtMensagem;
  boolean isAtivo = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    txtLocal = (TextView) findViewById(R.id.txtLocal);
    txtRemoto = (EditText) findViewById(R.id.txtRemoto);
    txtChat = (EditText) findViewById(R.id.txtChat);
    txtMensagem = (EditText) findViewById(R.id.txtMensagem);

    WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    int numero = wifiManager.getConnectionInfo().getIpAddress();
    String ip = Formatter.formatIpAddress(numero);
    txtLocal.setText(ip);
  }

  public void onClickEnviar(View v) {
    String mensagem = txtMensagem.getText().toString();
    String servidor = txtRemoto.getText().toString();
    txtChat.append("Eu: " + mensagem + "\n");
    MensageiroTask task = new MensageiroTask();
    task.execute(mensagem, servidor, String.valueOf(PORTA));
  }

  @Override
  protected void onResume() {
    super.onResume();
    isAtivo = true;
    new Thread(new BackgroundThread()).start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    isAtivo = false;
  }

  private class BackgroundThread implements Runnable {
    @Override
    public void run() {

      ServerSocket serverSocket = null;
      try {
        serverSocket = new ServerSocket(PORTA);
        int contador = 1;

        while (isAtivo) {

          Socket socket = null;
          DataInputStream dis = null;

          try {

            Log.d("WhatChat", "Aguardando conexão!");
            socket = serverSocket.accept();

            Log.d("WhatChat", "Repetição: " + contador++);

            dis = new DataInputStream(socket.getInputStream());

            try {
              final String host = socket.getInetAddress().getHostName();
              final String mensagem = dis.readUTF();
              if (mensagem != null && !mensagem.equals("")) {
                runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                    txtChat.append("Convidado: " + mensagem + "\n");
                  }
                });
              }
            } catch (IOException e) {
              //Igonorado;
            }

          } catch (IOException e) {
            e.printStackTrace();
          } finally {

            if (dis != null) {
              try {
                dis.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }

            if (socket != null) {
              try {
                socket.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      } finally {

        if (serverSocket != null) {
          try {
            serverSocket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  private class MensageiroTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... params) {

      String mensagemEnviar = params[0];
      String servidor = params[1];
      int porta = Integer.parseInt(params[2]);

      Socket socket = null;
      DataOutputStream dos = null;

      try {

        socket = new Socket(servidor, porta);
        dos = new DataOutputStream(socket.getOutputStream());
        dos.writeUTF(mensagemEnviar);

      } catch (IOException e) {
        e.printStackTrace();
      } finally {

        if (dos != null) {
          try {
            dos.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (socket != null) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return null;
    }
  }
}